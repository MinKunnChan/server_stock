package min.dbs.technicalchallenge.service;

import min.dbs.technicalchallenge.entity.StockEntity;
import min.dbs.technicalchallenge.model.StockRequest;
import min.dbs.technicalchallenge.model.StockRespond;
import min.dbs.technicalchallenge.repository.StockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StockServiceImplTest {

    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockServiceImpl stockService;

    StockEntity stock;
    StockRequest request;
    List<StockEntity> stocks;

    @BeforeEach
    void beforeEach() {
        stock = new StockEntity();
        stock.setSymbol("Test");
        stock.setId(1L);
        stock.setEventTime(new Date());
        request = new StockRequest();
        stocks = Arrays.asList(stock);
    }

    @Test
    void addOrUpdateStockWithNoSymbolWillReturnAllStocks() {

        when(stockRepository.findAll()).thenReturn(stocks);
        StockRespond stockRespond = stockService.addOrUpdateStock(request);

        verify(stockRepository).findAll();
        assertThat(stockRespond).isNotNull();
        assertThat(stockRespond.getStocks()).hasSize(1);
    }

    @Test
    void addOrUpdateStockCallSaveMethodAndWillReturnAllStocks() {

        request.setSymbol("Test");
        request.setBid(BigDecimal.ONE);
        request.setAsk(BigDecimal.ONE);

        when(stockRepository.findAll()).thenReturn(stocks);
        when(stockRepository.findBySymbol("Test")).thenReturn(Optional.of(stock));

        StockRespond stockRespond = stockService.addOrUpdateStock(request);
        verify(stockRepository).findBySymbol(stock.getSymbol());
        verify(stockRepository).findAll();
        verify(stockRepository).save(stock);

        assertThat(stockRespond).isNotNull();
        assertThat(stockRespond.getStocks()).hasSize(1);
    }

}