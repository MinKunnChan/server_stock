CREATE DATABASE  IF NOT EXISTS `stock_value`;
USE `stock_value`;

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ask` decimal(19,2) DEFAULT NULL,
  `bid` decimal(19,2) DEFAULT NULL,
  `event_time` datetime DEFAULT NULL,
  `symbol` varchar(255) NOT NULL,
  `trend` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_a86fxhchlsurngwa05iqbcw7b` (`symbol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `stock` VALUES
(1,10.00,10.00,'2020-06-30 14:56:05','D05:SGX', 'UP');
INSERT INTO `stock` VALUES
(2,9.00,9.00,'2020-06-30 14:56:15','O39:SGX', 'UP');
INSERT INTO `stock` VALUES
(3,10.00,10.00,'2020-06-30 14:56:25','U11:SGX', 'UP');

-- Dump completed on 2020-06-30