package min.dbs.technicalchallenge.service;

import min.dbs.technicalchallenge.model.StockRequest;
import min.dbs.technicalchallenge.model.StockRespond;

public interface StockService {

    StockRespond addOrUpdateStock(StockRequest stock);
}
