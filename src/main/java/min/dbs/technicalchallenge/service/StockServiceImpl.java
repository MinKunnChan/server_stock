package min.dbs.technicalchallenge.service;

import lombok.extern.slf4j.Slf4j;
import min.dbs.technicalchallenge.entity.StockEntity;
import min.dbs.technicalchallenge.model.Stock;
import min.dbs.technicalchallenge.model.StockRequest;
import min.dbs.technicalchallenge.model.StockRespond;
import min.dbs.technicalchallenge.repository.StockRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class StockServiceImpl implements StockService {

    private final StockRepository stockRepository;

    public StockServiceImpl(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Transactional
    @Override
    public StockRespond addOrUpdateStock(StockRequest stockRequest) {

        if("".equals(stockRequest.getSymbol())){
            log.info("addOrUpdateStock getAllStocks StockRequest {}", stockRequest);
            List<StockEntity> stocks = stockRepository.findAll();
            return convertToRespond(stocks);
        }

        log.info("addOrUpdateStock StockRequest {}", stockRequest);

        Optional<StockEntity> optionalStockEntity = stockRepository.findBySymbol(stockRequest.getSymbol());

        StockEntity foundEntity;
        if(optionalStockEntity.isPresent()){
            foundEntity = optionalStockEntity.get();
            BigDecimal newPrice = getPrice(foundEntity.getAsk(), foundEntity.getBid());
            BigDecimal originalPrice = getPrice(stockRequest.getAsk(), stockRequest.getBid());

            if(originalPrice.compareTo(newPrice) > 0){
                foundEntity.setTrend("UP");
            }else{
                foundEntity.setTrend("DOWN");
            }

        }else {
            foundEntity = new StockEntity();
            foundEntity.setTrend("UP");
        }

        foundEntity.setAsk(stockRequest.getAsk());
        foundEntity.setBid(stockRequest.getBid());
        foundEntity.setSymbol(stockRequest.getSymbol());
        foundEntity.setEventTime(new Date());

        stockRepository.save(foundEntity);
        List<StockEntity> stocks = stockRepository.findAll();
        return convertToRespond(stocks);
    }

    private StockRespond convertToRespond( List<StockEntity> stocks){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

        StockRespond stockRespond = new StockRespond();
        List<Stock> list = new ArrayList<>();
        for (StockEntity stockEntity: stocks) {
            Stock respond = new Stock();
            respond.setAsk(stockEntity.getAsk());
            respond.setBid(stockEntity.getBid());
            respond.setSymbol(stockEntity.getSymbol());
            respond.setPrice(getPrice(stockEntity.getAsk(), stockEntity.getBid()));

            String strDate = dateFormat.format(stockEntity.getEventTime());
            respond.setEventTime(strDate);
            respond.setTrend(stockEntity.getTrend());

            list.add(respond);
        }
        stockRespond.setStocks(list);
        return stockRespond;
    }

    private BigDecimal getPrice(BigDecimal ask, BigDecimal bid){
           return ask.add(bid)
                .divide(BigDecimal.valueOf(2));
    }

}
