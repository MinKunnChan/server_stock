package min.dbs.technicalchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealtimestockApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealtimestockApplication.class, args);
	}

}
