package min.dbs.technicalchallenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "stocks")
@Data
public class StockRespond {

    List<Stock> stocks = new ArrayList<>();
}
