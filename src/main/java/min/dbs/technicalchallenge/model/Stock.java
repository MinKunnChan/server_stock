package min.dbs.technicalchallenge.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class Stock {

    private String symbol;
    private BigDecimal ask;
    private BigDecimal bid;

    private BigDecimal price;
    private String eventTime;
    private String trend;

}
