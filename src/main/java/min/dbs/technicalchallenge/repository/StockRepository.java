package min.dbs.technicalchallenge.repository;

import min.dbs.technicalchallenge.entity.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<StockEntity, Long> {

    Optional<StockEntity> findBySymbol(String symbol);
}
