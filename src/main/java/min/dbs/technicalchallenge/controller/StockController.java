package min.dbs.technicalchallenge.controller;

import lombok.extern.slf4j.Slf4j;
import min.dbs.technicalchallenge.model.StockRequest;
import min.dbs.technicalchallenge.model.StockRespond;
import min.dbs.technicalchallenge.service.StockService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class StockController {

    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @MessageMapping("/stock")
    @SendTo("/topic/stocks")
    public StockRespond addOrUpdateStock(StockRequest stock) {
        log.info("stock => {}", stock);
        return stockService.addOrUpdateStock(stock);
    }
}
