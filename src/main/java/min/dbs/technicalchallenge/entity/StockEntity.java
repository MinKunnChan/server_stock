package min.dbs.technicalchallenge.entity;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@ToString
@Data
@Entity
@Table(name = "stock")
public class StockEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String symbol;

    @Column(nullable = false, length = 5)
    private String trend = "UP";

    private BigDecimal bid = BigDecimal.ZERO;
    private BigDecimal ask = BigDecimal.ZERO;
    private Date eventTime;

}
